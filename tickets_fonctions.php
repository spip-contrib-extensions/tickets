<?php

/**
 * Plugin Tickets
 * Licence GPL (c) 2008-2013
 *
 * @package SPIP\Tickets\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 *
 * Crée la liste des options du champ select d'assignation de ticket
 * Prend deux arguments optionnels :
 * -* $en_cours qui est l'id_auteur que l'on souhaite préselectionner
 * -* $format qui permet de choisir le format que la fonction renvoit :
 * -** rien ou autre chose que 'array' renverra les options d'un select
 * -** 'array' renverra un array des auteurs possibles
 *
 * @param int $en_cours
 * @param string $format
 * @return array|string
 */
function tickets_select_assignation($en_cours = 0, $format = 'select') {
	$options = null;
	$liste_assignables = [];

	include_spip('tickets_autorisations');
	$select = ['nom','id_auteur'];
	$from = ['spip_auteurs AS t1'];

	$autorises = [];
	$type = lire_config('tickets/autorisations/assigneretre_type');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorises['statut'] = ['0minirezo'];
				$autorises['webmestre'] = 'oui';
				break;
			case 'par_statut':
				$autorises['statut'] = lire_config('tickets/autorisations/assigneretre_statuts', ['0minirezo']);
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorises['auteur'] = lire_config('tickets/autorisations/assigneretre_auteurs', []);
				break;
		}
	}
	if (!$autorises) {
		$autorises = definir_autorisations_tickets('assigneretre');
	}

	if ($autorises['statut']) {
		$where = [sql_in('t1.statut', $autorises['statut']), 't1.email LIKE ' . sql_quote('%@%')];
		if (($autorises['webmestre'] ?? '') === 'oui') {
			$where[] = 't1.webmestre = ' . sql_quote('oui');
		}
	} else {
		$where = [sql_in('t1.id_auteur', $autorises['auteur']), 't1.email LIKE ' . sql_quote('%@%')];
	}

	$query_auteurs = sql_select($select, $from, $where);
	while ($row_auteur = sql_fetch($query_auteurs)) {
		$liste_assignables[$row_auteur['id_auteur']] = $row_auteur['nom'];
		$selected = ($row_auteur['id_auteur'] == $en_cours) ? ' selected="selected"' : '';
		$options .= '<option value="' . $row_auteur['id_auteur'] . '"' . $selected . '>' . $row_auteur['nom'] . '</option>';
	}
	if ($format == 'array') {
		return $liste_assignables;
	}

	return $options;
}

// fonction de selection de texte
function tickets_texte_statut($valeur) {
	$type = tickets_liste_statut();
	if (isset($type[$valeur])) {
		return $type[$valeur];
	}
}

function tickets_icone_statut($niveau, $full = false) {
	$img = [
		'ouvert' => 'puce-orange.gif',
		'resolu' => 'puce-verte.gif',
		'ferme' => 'puce-poubelle.gif',
		'poubelle' => 'puce-poubelle.gif'
		];
	if ($full) {
		return '<img src="' . find_in_path('prive/images/' . $img[$niveau]) . '" alt="' . tickets_texte_statut($niveau) . '" />';
	} else { return $img[$niveau];
	}
}

/**
 * Lister les statuts affichés dans les sélecteurs
 *
 * On teste les autorisations à instituer à ce niveau de statut
 *
 * @param int $id_ticket
 * 		L'identifiant numérique du ticket
 * @return array $statuts
 * 		La liste des statuts autorisés
 */
function tickets_liste_statut($id_ticket = null) {
	$statuts = [
		'ouvert' => _T('tickets:statut_ouvert'),
		'resolu' => _T('tickets:statut_resolu'),
		'ferme' => _T('tickets:statut_ferme'),
		'poubelle' => _T('tickets:statut_poubelle')
	];
	include_spip('inc/autoriser');
	foreach ($statuts as $statut => $titre) {
		if (!autoriser('instituer', 'ticket', $id_ticket, $GLOBALS['visiteur_session'], ['statut' => $statut])) {
			unset($statuts[$statut]);
		}
	}
	return $statuts;
}

/* Critere {mots_pargroupe} : "l'article est lie a au moins un mot de chacun des groupes demandes"
 * Ne s'applique que si au moins un mot est demande (si le tableau est vide, on ignore le critère)
 * On ignore la présence de '?' dans le critère
 */
function critere_mots_pargroupe_dist($idb, &$boucles, $crit, $id_ou_titre = false) {

	$boucle = &$boucles[$idb];

	// On ne prend pas en compte '?' mais on s'assure de récupérer le paramètre suivant s'il existe
	// le cas problématique : {mots_pargroupe ? #GET{tableau}}
	$num_param = 0;
	if ($crit->cond && isset($crit->param[0][0]) && $crit->param[0][0]->type === 'texte' && trim($crit->param[0][0]->texte) === '') {
		$num_param = 1;
	}
	if (isset($crit->param[0][$num_param])) {
		$mots = calculer_liste([$crit->param[0][$num_param]], [], $boucles, $boucles[$idb]->id_parent);
	} else {
		$mots = '@$Pile[0]["mots"]';
	}

	$boucle->hash .= '
	// {MOTS}
	$prepare_mots_pargroupe = charger_fonction(\'prepare_mots_pargroupe\', \'inc\');
	$mots_where = $prepare_mots_pargroupe(' . $mots . ', "' . $boucle->id_table . '");
	';

	$t = $boucle->id_table . '.' . $boucle->primary;
	if (!in_array($t, $boucles[$idb]->select)) {
	  $boucle->select[] = $t; # pour postgres, neuneu ici
	}

	$boucle->where[] = "\n\t\t" . '$mots_where';
}
function inc_prepare_mots_pargroupe_dist($mots, $table = 'articles') {

	// Si le tableau $mots est vide, on ignore le critère
	if (
		!is_array($mots)
		|| (!$mots = array_filter($mots))
	) {
		return '';
	}

	// Quel est l'objet ?
	$_table = table_objet($table);
	$objet_delatable = objet_type($_table);
	$_id_table = id_table_objet($table);
	// Tables spip_mots et spip_mots_liens
	$table_spip_mots = table_objet_sql('mots');
	$table_spip_mots_liens = table_objet_sql('mots_liens');

	/* On calcule la liste des groupes
	 *
	 * SELECT id_groupe
	 * FROM spip_mots
	 * WHERE id_mot IN (1,2,3)
	 * GROUP BY id_groupe
	 */
	$groupes = '(' . sql_get_select('id_groupe', $table_spip_mots, sql_in('id_mot', $mots), 'id_groupe') . ') AS g';

	/* Maintenant le nombre de groupes
	 *
	 * SELECT COUNT(id_groupe)
	 * FROM ($groupes) AS nb
	 */
	$nb_groupes = '(' . sql_get_select('COUNT(id_groupe)', $groupes) . ')';

	/* Maintenant les doublets (id_objet,id_groupe)
	 *
	 * attention : on écrit directement JOIN dans le code, peut être
	 * qu'il faudrait faire plus gaffe à la compatibilité SQL ?
	 *
	 * SELECT ml.id_objet,m.id_groupe
	 * FROM spip_mots_liens AS ml
	 * JOIN spip_mots AS m USING (id_mot)
	 * WHERE ml.objet='ticket'
	 *   AND ml.id_mot IN (1,2,3)
	 * GROUP BY ml.id_objet,ml.objet,m.id_groupe
	 */
	$doublets = '(' . sql_get_select('id_objet,id_groupe', $table_spip_mots_liens . ' JOIN ' . $table_spip_mots . ' USING (id_mot)', 'objet=' . sql_quote($objet_delatable) . ' AND ' . sql_in('id_mot', $mots), 'id_objet,objet,id_groupe') . ') AS d';

	/* Enfin, la boucle complete
	 * SELECT id_objet
	 * FROM (
	 *   SELECT id_objet,id_groupe
	 *   FROM spip_mots_liens AS ml
	 *   JOIN spip_mots AS m USING (id_mot)
	 *   WHERE objet='ticket'
	 *     AND id_mot IN (1,2,3)
	 *   GROUP BY id_objet,objet,id_groupe
	 * ) AS d
	 * GROUP BY id_objet
	 * HAVING SUM(1) >= (
	 *   SELECT COUNT(*)
	 *   FROM (
	 *     SELECT id_groupe
	 *     FROM spip_mots
	 *     WHERE id_mot IN (1,2,3)
	 *     GROUP BY id_groupe
	 *   ) AS g
	 * )
	 */
	$where = sql_get_select('id_objet', $doublets, '', 'id_objet', '', '', 'SUM(1) >= ' . $nb_groupes);

	/* On ajoute ce critère à la boucle (TICKETS)
	 *
	 */
	return $_table . '.' . $_id_table . ' IN (' . $where . ')';
}

function tickets_motslies_groupe($id_groupe, $id_ticket) {

	$texte = '';
	$from = ['spip_mots_liens AS l', 'spip_mots AS m'];
	$select = ['m.titre AS titre'];
	$where = [
				'l.objet=' . sql_quote('ticket'),
				'l.id_objet=' . intval($id_ticket),
				'm.id_groupe=' . intval($id_groupe),
				'm.id_mot=l.id_mot'
	];
	$mots = sql_allfetsel($select, $from, $where);
	$mots = array_column($mots, 'titre');
	$mots = array_map('supprimer_numero', $mots);

	$texte = implode(', ', $mots);

	return $texte;
}
