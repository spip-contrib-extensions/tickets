<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tickets?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tickets_description' => '{Lístky} sú nástrojom na sledovanie chýb priamo začleneným do súkromnej zóny SPIPu. Sú vytvorené na pomoc pri vývoji prvkov stránky (vrátane šablón verejne prístupnej stránky).', # MODIF
	'tickets_nom' => 'Lístky',
	'tickets_slogan' => 'Systém na sledovanie chýb' # MODIF
);
