<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tickets?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tickets_description' => '{Tickets} è uno strumento di tracciamento delle attività che può essere utilizzato sia nell’area privata SPIP che nel sito pubblico. Ad esempio può essere utilizzato per facilitare la fase di sviluppo del sito (compresa la struttura del sito pubblico) o tracciare i bug.',
	'tickets_nom' => 'Tickets',
	'tickets_slogan' => 'Sistema di tracciamento delle attività'
);
