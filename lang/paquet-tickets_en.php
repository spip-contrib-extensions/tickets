<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tickets?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tickets_description' => '{Bug reports} is a bug tracking tool integrated into SPIP, both in the private and public area. It is designed to facilitate the development phase of the site, but it can also be used for bug tracking on an operational site.',
	'tickets_nom' => 'Bug reports',
	'tickets_slogan' => 'Bug tracking system'
);
