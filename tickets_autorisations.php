<?php

/**
 * Plugin Tickets
 * Licence GPL (c) 2008-2013
 *
 * @package SPIP\Tickets\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction pour le pipeline, n'a rien a effectuer
 *
 * @return void
 */
function tickets_autoriser() {
}

/**
 * Renvoie la liste des auteurs ou des statuts autorises pour une action donnee
 *
 * @param string $action
 * 		L'action que l'on souhaite faire
 * @param boolean $utiliser_defaut [optional]
 * @return array
 */
function definir_autorisations_tickets($action, $utiliser_defaut = true) {
	$aut = null;

	switch (strtolower($action)) {
		case 'ecrire':
			$define = (defined('_TICKETS_AUTORISATION_ECRIRE')) ? _TICKETS_AUTORISATION_ECRIRE : ($utiliser_defaut ? '0minirezo' : '');
			break;
		case 'notifier':
			$define = (defined('_TICKETS_AUTORISATION_NOTIFIER')) ? _TICKETS_AUTORISATION_NOTIFIER : ($utiliser_defaut ? definir_notifications_tickets() : '');
			break;
		case 'assigner':
			$define = (defined('_TICKETS_AUTORISATION_ASSIGNER')) ? _TICKETS_AUTORISATION_ASSIGNER : ($utiliser_defaut ? '0minirezo' : '');
			break;
		case 'assigneretre':
			$define = (defined('_TICKETS_AUTORISATION_ASSIGNERETRE')) ? _TICKETS_AUTORISATION_ASSIGNERETRE : ($utiliser_defaut ? '0minirezo' : '');
			break;
		case 'commenter':
			$define = (defined('_TICKETS_AUTORISATION_COMMENTER')) ? _TICKETS_AUTORISATION_COMMENTER : ($utiliser_defaut ? '1comite' : '');
			break;
		case 'modifier':
			$define = (defined('_TICKETS_AUTORISATION_MODIFIER')) ? _TICKETS_AUTORISATION_MODIFIER : ($utiliser_defaut ? '0minirezo' : '');
			break;
		case 'epingler':
			$define = (defined('_TICKETS_AUTORISATION_EPINGLER')) ? _TICKETS_AUTORISATION_EPINGLER : ($utiliser_defaut ? '0minirezo' : '');
			break;
		default:
			$define = $utiliser_defaut ? '0minirezo' : '';
			break;
	}

	if ($define) {
		$liste = explode(':', $define);
		if (in_array('webmestre', $liste)) {
			$webmestres = sql_allfetsel('id_auteur', 'spip_auteurs', 'webmestre=' . sql_quote('oui'));
			if (defined('_ID_WEBMESTRES')) {
				$aut['auteur'] = array_merge($webmestres, explode(':', _ID_WEBMESTRES));
			} else {
				$aut['auteur'] = $webmestres;
			}
		} elseif (in_array('0minirezo', $liste)) {
			$aut['statut'] = ['0minirezo'];
		} elseif (in_array('1comite', $liste)) {
			$aut['statut'] = ['0minirezo', '1comite'];
		} elseif (in_array('6forum', $liste)) {
			$aut['statut'] = ['0minirezo', '1comite', '6forum'];
		} else {
			$aut['auteur'] = $liste;
		}
	}

	return $aut;
}

/**
 * Destinataires des notifications de publication des tickets
 * @return string
*/
function definir_notifications_tickets() {
	$cfg = lire_config('tickets/general/notif_destinataires');
	if (in_array($cfg, ['1comite', '0minirezo', 'webmestre'])) {
		return $cfg;
	} elseif ($cfg == 'liste') {
		return implode(':', lire_config('tickets/general/notif_auteurs'));
	}
	else {
		return '';
	}
}
/**
 * Autorisation d'écrire des tickets
 * (définit qui peut créer un ticket)
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_ecrire_dist($faire, $type, $id, $qui, $opt) {
	if ($associer_objet = $opt['associer_objet'] ?? '') {
		if (intval($associer_objet)) {
			// compat avec l'appel de la forme ajouter_id_article
			$objet = 'article';
			$id_objet = intval($associer_objet);
		}
		else {
			list($objet, $id_objet) = explode('|', $associer_objet);
		}
		if (!autoriser('associertickets', $objet, $id_objet, $qui)) {
			return false;
		}
	}

	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (autoriser('modifier', $type, $id, $qui, $opt)) {
		return autoriser('modifier', $type, $id, $qui, $opt);
	}

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	$type = lire_config('tickets/autorisations/ecrire_type');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if (in_array('tous', lire_config('tickets/autorisations/ecrire_statuts', ['0minirezo']))) {
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/ecrire_statuts', []));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/ecrire_auteurs', []));
				break;
		}
		if ($autorise == true) {
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si pas configuré ou pas autorisé dans la conf => on teste les define
	$liste = definir_autorisations_tickets('ecrire', $utiliser_defaut);
	if ($liste && $liste['statut']) {
		$autorise = in_array($qui['statut'], $liste['statut']);
	} elseif ($liste && $liste['auteur']) {
		$autorise = in_array($qui['id_auteur'], $liste['auteur']);
	}

	return $autorise;
}


/**
 * Autorisation de créer des tickets
 * (définit qui peut créer un ticket)
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_creer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('ecrire', 'ticket', $id, $qui, $opt);
}

/**
 * Autorisation d'assignation des tickets
 * (définit qui peut assigner les tickets)
 *
 * - Les webmestres
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_assigner_dist($faire, $type, $id, $qui, $opt) {
	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	if ((lire_config('tickets/autorisations/assigner_modifieur') == 'on') && autoriser('modifier', $type, $id, $qui, $opt)) {
		return true;
	}

	$type = lire_config('tickets/autorisations/assigner_type');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if (in_array('tous', lire_config('tickets/autorisations/assigner_statuts', []))) {
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/assigner_statuts', ['0minirezo']));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/assigner_auteurs', []));
				break;
		}
		if ($autorise == true) {
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	$liste = definir_autorisations_tickets('assigner', $utiliser_defaut);
	if ($liste && $liste['statut']) {
		$autorise = in_array($qui['statut'], $liste['statut']);
	} elseif ($liste && $liste['auteur']) {
		$autorise = in_array($qui['id_auteur'], $liste['auteur']);
	}

	return $autorise;
}

/**
 * Autorisation d'être assigné à un ticket
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_assigneretre_dist($faire, $type, $id, $qui, $opt) {

	$autorise = false;
	$utiliser_defaut = true;

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	$type = lire_config('tickets/autorisations/assigneretre_type');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if (in_array('tous', lire_config('tickets/autorisations/assigneretre_statuts', []))) {
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/assigneretre_statuts', ['0minirezo']));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/assigneretre_auteurs', []));
				break;
		}
		if ($autorise == true) {
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	$liste = definir_autorisations_tickets('assigneretre', $utiliser_defaut);
	if ($liste['statut']) {
		$autorise = in_array($qui['statut'], $liste['statut']);
	} elseif ($liste['auteur']) {
		$autorise = in_array($qui['id_auteur'], $liste['auteur']);
	}

	return $autorise;
}

/**
 * Autorisation de modification des tickets
 * Définit qui peut modifier les tickets :
 * - Les webmestres
 * - L'auteur du ticket
 * - Les personnes assignées
 * - Les personnes correspondant à la configuration
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_modifier_dist($faire, $type, $id, $qui, $opt) {
	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (is_numeric($id)) {
		// Si l'auteur en question est l'auteur du ticket ou l'auteur assigné au ticket,
		// il peut modifier le ticket
		$id_assigne_auteur = sql_fetsel('id_assigne,id_auteur', 'spip_tickets', 'id_ticket=' . intval($id));
		if (
			($id_assigne_auteur && $id_assigne_auteur['id_auteur'] && ($id_assigne_auteur['id_auteur'] == $qui['id_auteur']))
			 || ($id_assigne_auteur && $id_assigne_auteur['id_assigne'] && ($id_assigne_auteur['id_assigne'] == $qui['id_auteur']))
		) {
			return true;
		}

		if (!function_exists('lire_config')) {
			include_spip('inc/config');
		}

		$type = lire_config('tickets/autorisations/modifier_type', 'par_statut');
		if ($type) {
			switch ($type) {
				case 'webmestre':
					// Webmestres uniquement
					$autorise = ($qui['webmestre'] == 'oui');
					break;
				case 'par_statut':
					// Traitement spécifique pour la valeur 'tous'
					if (in_array('tous', lire_config('tickets/autorisations/modifier_statuts', []))) {
						return true;
					}
					// Autorisation par statut
					$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/modifier_statuts', ['0minirezo']));
					break;
				case 'par_auteur':
					// Autorisation par id d'auteurs
					$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/modifier_auteurs', []));
					break;
			}
			if ($autorise == true) {
				return $autorise;
			}
			$utiliser_defaut = false;
		}

		// Si $utiliser_defaut = true, on utilisera les valeurs par défaut
		// Sinon on ajoute la possibilité de régler par define
		$liste = definir_autorisations_tickets('modifier', $utiliser_defaut);
		if ($liste && $liste['statut']) {
			$autorise = in_array($qui['statut'], $liste['statut']);
		} elseif ($liste && $liste['auteur']) {
			$autorise = in_array($qui['id_auteur'], $liste['auteur']);
		}
		if (!$autorise) {
			$id_auteur = sql_getfetsel('id_auteur', 'spip_tickets', 'id_ticket=' . intval($id));
			if ($id_auteur && ($id_auteur == $qui['id_auteur'])) {
				$autorise = true;
			}
		}
	}
	return $autorise;
}

/**
 * Autorisation à instituer des tickets
 * Définit qui peut changer le statut des tickets :
 *
 * - les webmestres
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_instituer_dist($faire, $type, $id, $qui, $opt) {
	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (is_numeric($id)) {
		// Si l'auteur en question est l'auteur du ticket ou l'auteur assigné au ticket,
		// il peut instituer le ticket
		$id_assigne_auteur = sql_fetsel('statut,id_assigne,id_auteur', 'spip_tickets', 'id_ticket=' . intval($id));

		if (
			($id_assigne_auteur['statut'] == 'redac' && $opt['statut'] == 'ouvert')
			|| ($id_assigne_auteur['id_auteur'] && ($id_assigne_auteur['id_auteur'] == $qui['id_auteur']))
			|| ($id_assigne_auteur['id_assigne'] && ($id_assigne_auteur['id_assigne'] == $qui['id_auteur']))
		) {
			return true;
		}

		if (!function_exists('lire_config')) {
			include_spip('inc/config');
		}

		$type = lire_config('tickets/autorisations/modifier_type', 'par_statut');
		if ($type) {
			switch ($type) {
				case 'webmestre':
					// Webmestres uniquement
					$autorise = ($qui['webmestre'] == 'oui');
					break;
				case 'par_statut':
					// Traitement spécifique pour la valeur 'tous'
					if (in_array('tous', lire_config('tickets/autorisations/modifier_statuts', []))) {
						return true;
					}
					// Autorisation par statut
					$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/modifier_statuts', ['0minirezo']));
					break;
				case 'par_auteur':
					// Autorisation par id d'auteurs
					$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/modifier_auteurs', []));
					break;
			}
			if ($autorise == true) {
				return $autorise;
			}
			$utiliser_defaut = false;
		}

		// Si $utiliser_defaut = true, on utilisera les valeurs par défaut
		// Sinon on ajoute la possibilité de régler par define
		$liste = definir_autorisations_tickets('modifier', $utiliser_defaut);
		if ($liste && $liste['statut']) {
			$autorise = in_array($qui['statut'], $liste['statut']);
		} elseif ($liste && $liste['auteur']) {
			$autorise = in_array($qui['id_auteur'], $liste['auteur']);
		}
		if (!$autorise) {
			$id_auteur = sql_getfetsel('id_auteur', 'spip_tickets', 'id_ticket=' . intval($id));
			if ($id_auteur && ($id_auteur == $qui['id_auteur'])) {
				$autorise = true;
			}
		}
	}
	return $autorise;
}

/**
 * Autorisation de commenter des tickets
 * (définit qui doit être notifié)
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_commenter_dist($faire, $type, $id, $qui, $opt) {
	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	if ((lire_config('tickets/autorisations/commenter_modifieur') == 'on') && autoriser('modifier', $type, $id, $qui, $opt)) {
		return true;
	}

	$type = lire_config('tickets/autorisations/commenter_type', 'par_statut');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if (in_array('tous', lire_config('tickets/autorisations/commenter_statuts', []))) {
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/commenter_statuts', ['0minirezo','1comite']));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/commenter_auteurs', []));
				break;
		}
		if ($autorise == true) {
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	$liste = definir_autorisations_tickets('commenter', $utiliser_defaut);
	if ($liste['statut']) {
		$autorise = in_array($qui['statut'], $liste['statut']);
	} elseif ($liste['auteur']) {
		$autorise = in_array($qui['id_auteur'], $liste['auteur']);
	}

	return $autorise;
}

/**
 * Autorisation d'épingler des tickets
 * Définit qui peut épingler les tickets
 * Par défaut seulement les admins
 *
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true|false : true si autorisé, false sinon
 */
function autoriser_ticket_epingler_dist($faire, $type, $id, $qui, $opt) {
	if (($qui['webmestre'] == 'oui') && $qui['statut'] == '0minirezo') {
		return true;
	}

	$autorise = false;
	$utiliser_defaut = true;

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	$type = lire_config('tickets/autorisations/epingler_type', 'par_statut');
	if ($type) {
		switch ($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if (in_array('tous', lire_config('tickets/autorisations/epingler_statuts', ['0minirezo']))) {
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('tickets/autorisations/epingler_statuts', ['0minirezo']));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('tickets/autorisations/epingler_auteurs', []));
				break;
		}
		if ($autorise == true) {
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si $utiliser_defaut = true, on utilisera les valeurs par défaut
	// Sinon on ajoute la possibilité de régler par define
	$liste = definir_autorisations_tickets('epingler', $utiliser_defaut);
	if ($liste['statut']) {
		$autorise = in_array($qui['statut'], $liste['statut']);
	} elseif ($liste['auteur']) {
		$autorise = in_array($qui['id_auteur'], $liste['auteur']);
	}
	return $autorise;
}

/**
 * Un ticket peut ̂être vu par tout le monde
 *
 * @param string $faire
 * 		L'action à faire
 * @param string $type
 * 		Le type d'objet sur lequel porte l'action
 * @param int $id
 *		L'identifiant numérique de l'objet
 * @param array $qui
 * 		Les éléments de session de l'utilisateur en cours
 * @param array $opt
 * 		Les options
 * @return boolean true|false
 * 		true si autorisé, false sinon
 */
function autoriser_ticket_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * La liste des tickets est accessible à tout le monde
 *
 * @param string $faire
 * 		L'action à faire
 * @param string $type
 * 		Le type d'objet sur lequel porte l'action
 * @param int $id
 *		L'identifiant numérique de l'objet
 * @param array $qui
 * 		Les éléments de session de l'utilisateur en cours
 * @param array $opt
 * 		Les options
 * @return boolean true|false
 * 		true si autorisé, false sinon
 */
function autoriser_tickets_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Le menu des tickets est accessible à tout ceux qui peuvent voir la lsite des tickets
 *
 * @param string $faire
 * 		L'action à faire
 * @param string $type
 * 		Le type d'objet sur lequel porte l'action
 * @param int $id
 *		L'identifiant numérique de l'objet
 * @param array $qui
 * 		Les éléments de session de l'utilisateur en cours
 * @param array $opt
 * 		Les options
 * @return boolean true|false
 * 		true si autorisé, false sinon
 */
function autoriser_tickets_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('voir', 'tickets');
}

/**
 * Le bouton de création rapide de ticket est visible pour ceux pouvant créer un ticket
 *
 * @param string $faire
 * 		L'action à faire
 * @param string $type
 * 		Le type d'objet sur lequel porte l'action
 * @param int $id
 *		L'identifiant numérique de l'objet
 * @param array $qui
 * 		Les éléments de session de l'utilisateur en cours
 * @param array $opt
 * 		Les options
 * @return boolean true|false
 * 		true si autorisé, false sinon
 */
function autoriser_ticketedit_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('ecrire', 'ticket');
}

/**
 * Autorisation d'associer des tickets à un objet
 *
 * A priori, on n'interdit pas d'associer des tickets à d'autres tickets
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_associertickets_dist($faire, $type, $id, $qui, $opt) {
	$table = table_objet($type);

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}
	$tables_liees = lire_config('tickets/general/tables_liees');
	if ($tables_liees && in_array($table, $tables_liees)) {
		return autoriser('modifier', $type, $id, $qui);
	}

	return false;
}
