# CHANGELOG

## 4.3.1 - 2025-02-25

### Fixed

- #26 éviter warning passage par référence + mise en forme du readme.md

## 4.3.0 - 2024-07-05

### Fixed

- Compatibilité SPIP 4.*

## 4.2.1 - 2023-11-28

### Fixed

- #11 Rétablir l'affichage du contenu des tickets

## 4.2.0 - 2023-11-24

### Fixed

- #1 Rétablir le classement par groupe des tickets
- #3 Faire fonctionner les champs extras
